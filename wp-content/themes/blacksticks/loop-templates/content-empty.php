<?php
/**
 * Content empty partial template
 *
 * @package blacksticks
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
